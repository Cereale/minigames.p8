pico-8 cartridge // http://www.pico-8.com
version 18
__lua__

easy_mode = 0
g = 0.002
n_mountains = 10
max_fuel = 60
fuel_consumption = 0.08

function _init()
	cls()
	fuel = max_fuel
	game_status = 1
	game_paused = 0
	winning_time = 10000000
	x_stars = {}
	y_stars = {}
	landing_spot = 2 + flr(rnd(n_mountains-1))
	x = (landing_spot + 64 + rnd(40))%128
	y = 0
	v_x = 0
	v_y = 0
	flare_right = 0
	flare_left = 0
	flare_down = 0
	music_on = 0
	if (easy_mode == 1) then
		crashing_speed = 0.30
	else
		crashing_speed = 0.20
	end

	for i=1,40 do
		add(x_stars, rnd(128))
		add(y_stars, rnd(100))
	end
	mountains_x, mountains_y, landscape, counter = build_mountains()
end



function _update()
	if (game_status==1) do
		x = min(max(x + v_x, 1), 120)
		y = y + v_y
		v_y += g

		-- inputs :
		if (btn(0) and fuel>0) then
			v_x -= 1.5*g
			flare_left = 1
			sfx(3, 3)
			fuel -= fuel_consumption
		else
			flare_left = 0
		end
		if (btn(1) and fuel>0) then
			v_x += 1.5*g
			flare_right = 1
			sfx(3, 3)
			fuel -= fuel_consumption
		else
			flare_right = 0
		end
		if (btn(2) and fuel>0) then
			v_y -= 1.5*g
			flare_down = 1
			sfx(3, 3)
			fuel -= fuel_consumption
		else
			flare_down = 0
		end

		if (music_on == 0 and time()>0) then
			start_music()
			music_on = 1
		end
	end

	print(landscape[i])
	for i=flr(x),(flr(x)+8)%128 do
		print(i)
		if (y+8 > landscape[i]) then
			if (not(x > mountains_x[landing_spot-1] and x < mountains_x[landing_spot]) or sqrt(v_y*v_y + v_x * v_x) > crashing_speed) then
				game_status = 0
				game_paused = 1
				start_music()
			else
				game_status = 2
				if game_paused == 0 then
					winning_time = flr(time() * 100)/100
				end
				game_paused = 1
				start_music()
			end
		end
	end

end


function _draw()
	cls()
	-- playing
	if (game_status == 1) then
		draw_stars(x_stars, y_stars)
		draw_mountains(mountains_x, mountains_y)
		draw_fuel()
		draw_player(x, y)
		if (easy_mode==1) then
			print("v_x ", 0, 0, 3)
			print(abs(flr(v_x * 100)/100), 15, 0, 3)
			print("v_y ", 0, 7, 3)
			print(abs(flr(v_y * 100)/100), 15, 7, 3)
		end
	end

	-- game over
	if (game_status == 0) then
		draw_game_over()
	end

	-- Success
	if (game_status == 2) then
		flare_down, flare_left, flare_right = 0, 0, 0
		draw_stars(x_stars, y_stars)
		draw_mountains(mountains_x, mountains_y)
		draw_player(x, y)
		draw_victory()
		draw_fuel()
	end

end

function draw_game_over()
	print("game over", 50, 30, 8)
	print(" A", 500, 40, 8)

end

function draw_victory()

	--print("SUCCESS", 50, 30, 11)

		print("success !", 50, 30, 11)
		print("SUCCESS", 500, 40, 11)
		print("score : ", 35, 40, 11)
		print(winning_time, 69, 40, 11)


end

function draw_player(x, y)
	spr(1, x, y)
	if (flare_left==1) then spr(4, x+2, y)
	end
	if (flare_right==1) then spr(3, x-2, y)
	end
	if (flare_down==1) then spr(2, x, y+1)
	end
end

function draw_fuel()
	rectfill(115, 30, 120, 30 + max_fuel, 0)
	rect(115, 30, 120, 30 + max_fuel, 1)
	for i=1, max_fuel do
		if (i<max_fuel/3 and i < fuel) then
			line(116, 30 + max_fuel -i, 119, 30 + max_fuel -i, 8)
		end
		if (max_fuel/3 < i and i < 2 * max_fuel/3 and i < fuel) then
			line(116, 30 + max_fuel -i, 119, 30 + max_fuel -i, 9)
		end
		if (2*max_fuel/3 < i and i < fuel) then
			line(116, 30 + max_fuel -i, 119, 30 + max_fuel -i, 10)
		end
	end
	print("F", 110, 41, 1)
	print("U", 110, 52, 1)
	print("E", 110, 63, 1)
	print("L", 110, 74	, 1)

end

function draw_stars(x_stars, y_stars)
	for i=1,30 do
		pset(x_stars[i], y_stars[i], 7)
	end
end

function build_mountains()
	mountains_x = {}
	mountains_y = {}
	landscape = {}
	last_x = 0
	last_y = 0
	delta_x = 0
	delta_y = 0

	-- making points
	for i=1,n_mountains do
		delta_x = 5 + rnd(4)
		delta_y = rnd(25)
		if (i==landing_spot)then
			delta_x = 20
			delta_y = 0
			add(mountains_y, last_y)
		else
			add(mountains_y, 123 - delta_y)
		end
		last_x += delta_x
		last_y = 123 - delta_y
		add(mountains_x, last_x)
	end

	-- making slopes
	counter = 0
	n = 1
	-- first slope
	for i=1,mountains_x[1]-1 do
		add(landscape, interpolate(i, 0, 128, mountains_x[1], mountains_y[1]))
		counter += 1
	end


	-- mountains
	for i=mountains_x[1],mountains_x[n_mountains]-1 do
		counter += 1
		if (i>=mountains_x[n])then
			n += 1
		end
		add(landscape, interpolate(i, mountains_x[n-1], mountains_y[n-1], mountains_x[n], mountains_y[n]))
	end

	--last slope
	for i=mountains_x[n_mountains], 129 do
		counter += 1
		add(landscape, interpolate(i, mountains_x[n_mountains], mountains_y[n_mountains], 128, 128))
	end
	print(counter)
	return mountains_x, mountains_y, landscape, counter
end


function draw_mountains(mountains_x, mountains_y)

	for i=1,128 do
		line(i, 128, i, landscape[i], 6)
	end

	line(mountains_x[landing_spot-1], mountains_y[landing_spot-1], mountains_x[landing_spot], mountains_y[landing_spot], 3)

end


function interpolate(x, x1, y1, x2, y2)
	a = (y1-y2) / (x1-x2)
	b = y1 - x1 * a
	return(a*x + b)
end


function start_music()
	if (game_status == 1) then
		stop_music()
		sfx(1, 1)
		music(1, 0, 0)
		sfx(0, 2, 0)
	end
	if (game_status == 2) then
		stop_music()
		music(4, 1, 0, 16)
	end
	if (game_status == 0) then
		stop_music()
		music(5, 1, 0, 16)
	end
end


function stop_music()
	sfx(-1, 0)
	sfx(-1, 1)
	sfx(-1, 2)
	sfx(-1, 3)
	music(-1, 0)
	music(-1, 1)
	music(-1, 2)
	music(-1, 3)

end
__gfx__
000000000066dd000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000066dd1100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0070070066ddd1150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000770006dd111550000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000770000dd115500000000008a0000000000a800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0070070000115500000aa00000800000000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000090000900089980000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000900000090008800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
00a000081575010750157500d7501475010750147500d750230001f70024000240002500026000260002600026000270001c0002800029000290002b0002c0002e0002f00030000310003300034000370003a000
011400200375001000030000600005750092000a20000000017500b2000d2000b200097500b2000c2000c20003750040000d20005000067503f3000e2000f20001750060000f2000f2000475002000000000f200
0001000017600206001c6001a6001d600266002a6002d6003160029600216001a6001460011600106000f000116001600015600160001b60016000226001f00029600170002f6001700033600190003460002600
000800001761000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0010000011750177501f750277501f70000000357000000000000000001b1000000000000000000000000000000002800000000000001f3000f50000000000000000000000000000000000000000002270000000
0010000019750147500e7500a7500a700097000b70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
