pico-8 cartridge // http://www.pico-8.com
version 18
__lua__




function _init()
 cls()
 acc = 0.03
 friction = acc/5
 g = acc/3
 x, y = 64, 1
 v_x, v_y = 0, 0
 n_x, n_y = flr(x/16), flr(y/16)
 dir = 0
 size = 7
 game_status = 1
 cargo = 0
 saved = 0
 total_to_save = 4 + flr(rnd(4))
 time_of_win = 100000
 game_paused = 0

 x_stars = {}
 y_stars = {}
 for i=1,40 do
		add(x_stars, rnd(128))
		add(y_stars, rnd(100))
	end

 buildings = make_buildings()
 buildings = make_people(buildings)
 buildings[1][4] = 5


end



function _update()

 --x = min(max(x + v_x, 0), 128-size)
 --y = min(max(y + v_y, 0), 128-size)
 x += v_x
 y += v_y
 if (x>(128-size) or x<1) then
  v_x *= -0.9
 end
 if (y>(127-size) or y<1) then
  v_y *= -0.9
 end

 n_x, n_y = flr(x/8) + 1, flr(y/8) + 2
 if (saved == total_to_save) then
  game_status = 2
  if (game_paused==0) then
   time_of_win = flr(time() * 100)/100
  end
  game_paused = 1
 end
 if (game_status==1) then
  -- input movement
  if (btn(0)) then
   v_x -= acc
   dir = 0
  end

  if (btn(1)) then
   v_x += acc
   dir = 1
  end
  if (btn(2)) then
   v_y -= acc
  end
  if (btn(3)) then
   v_y += acc
  end
  -- gravity
  v_y += g

  -- friction
  if (v_x > 0) then
   v_x -= friction
  else
   v_x += friction
  end
  if (v_y > 0) then
   v_y -= friction
  else
   v_y += friction
  end

  for i=1,16 do
   for j=1,16 do
    if (buildings[i][j] == 1) then
     if ((x+0.5)/8 < i and (y+0.5)/8 < j and (x + size)/8 > (i-1) and (y + size)/8 > (j-1)) then
      game_status = 0
     end
    end
    if (buildings[i][j] == 2) then
     if ((x+0.5)/8 < i and (y+0.5)/8 < j and (x + size)/8 > (i-1) and (y + size)/8 > (j-1)) then
      if (cargo==0) then
       cargo = 1
       buildings[i][j] = 1
       v_x = v_x
       v_y = -v_y
       x -= sgn(v_x)
       y -= 1
      else
       v_x = v_x
       v_y = -v_y
      end
     end
    end
    if (buildings[i][j] == 5) then
     if ((x+0.5)/8 < i and (y+0.5)/8 < j and (x + size)/8 > (i-1) and (y + size)/8 > (j-1)) then
      v_x = v_x
      v_y = v_y
      y -= 1
      if (cargo==1) then
       saved += 1
       cargo = 0
      end
     end
    end
   end
  end
 end
end


function _draw()
 if (game_status == 1) then
  cls()

  for i=1,30 do
 		pset(x_stars[i], y_stars[i], 7)
 	end

  for i = 1,16 do
   for j = 1,16 do
    if (buildings[i][j]==1) then

     spr(16, 8*(i-1), 8*(j-1))
    end
    if (buildings[i][j]==2) then
     --print(j,15)
     spr(17, 8*(i-1), 8*(j-1))
    end
    if (buildings[i][j]==5) then
     spr(2, 8*(i-1), 8*(j-1))
    end

   end
  end
  if (cargo == 0) then
   if (dir==1) then
    spr(0, x, y, 1, 1, true, false)
   else
    spr(0, x, y, 1, 1, false, false)
   end
  else
   if (dir==1) then
    spr(1, x, y, 1, 1, true, false)
   else
    spr(1, x, y, 1, 1, false, false)
   end
  end
  print(saved)
  --print(y/8, 15)
 end
 if (game_status == 0) then
  cls()
  print("game over", 50, 60, 8)
 end

 if (game_status == 2) then
  cls()
  print("you saved everyone", 30, 50, 3)
  print("congratulations!", 34, 40, 3)
  print("score : ", 45, 60, 3)
		print(time_of_win, 75, 60, 3)
 end
end

function make_people(buildings)
  for k=1,total_to_save do
   done = 0
   while (done==0) do
    void = 0
    j = 1
    i = flr(1+rnd(15))
    while (void==0 and j<16)do
     if (buildings[i][j] == 2) then
      void = 1
     end
     if (buildings[i][j]==1 and void==0) then
      void = 1
      buildings[i][j]=2
      done = 1
     end
     j+=1
    end
   end
  end
  return buildings
 end

function make_buildings()
 buildings = {}
 n_seeds = 8
 total_blocks = n_seeds - 1

 for i = 1,16 do
  buildings[i] = {}
  for j = 1,16 do
   buildings[i][j] = 0
  end
 end

 for i=1, n_seeds do
  d = 1 + flr(rnd(16))
  buildings[d][16] = 1
 end

 while (total_blocks < 16*2) do
  for i = 1,16 do
   j = 1
   void = 1
   while (void==1 or j<17) do
    if (buildings[i][j] == 1) then

     buildings = build(buildings, i, j)
     total_blocks += 1
    end
    j+=1
    if (j==16)then
     void = 0
    end
   end
  end
 end
 return buildings
end

function build(buildings, i, j)
 dice = flr(rnd(7))
 if (dice > 1 and j > 3 and buildings[i][j-1] == 0)then
  buildings[i][j-1] = 1
 end
 if (dice == 0 and j > 3 and i!=1 and buildings[i-1][j] == 0) then
  buildings[i-1][j] = 1
 end
 if (dice == 1 and j > 3 and i!=16 and buildings[i+1][j] == 0) then
  buildings[i+1][j] = 1
 end

 return buildings
end

function get_neighbours(buildings, i, j)
 n = 0
 if (buildings[(i-2)%16 + 1][j] == 1) then
  n += 1
 end
 if (buildings[(i+2)%15 - 1][j] == 1) then
  n += 1
 end
 if (buildings[i][(j-2)%16 + 1] == 1) then
  n += 1
 end
 if (buildings[i][(j+2)%16 - 1] == 1) then
  n += 1
 end
 return(n)
end

__gfx__
05d5d5d505d5d5d5bbbbbbbb00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00005000000050003333333300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
06605000066050003333333000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6cc6dd056cc6dd053333300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
666ddd5566fdfd553330000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
06ddd50006dfdf000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000d0000000d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
06ddd50006ddd5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5dddddd15dddddd10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1dddddd51dfdddd50000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5111ddd55111fdf50000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5ddd11155ddd11150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5dddddd15dddddd10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1dddddd51fdfddd50000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5111ddd55111dfd50000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5ddd11155ddd11150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__gff__
0000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
